import React from "react";
import "./Header.css";
import { BsSunFill } from "react-icons/bs";
import { TbMoonFilled } from "react-icons/tb";

const Header = ({ theme, setTheme }) => {
  const toggleTheme = () => {
    if (theme === "light") {
      setTheme("dark");
    } else {
      setTheme("light");
    }
  };
  return (
    <header>
      <div className="logo">
        <span>Task Management</span>
      </div>
      <div className="theme-container">
        <span>{theme}</span>
        <span className="icon" onClick={toggleTheme}>
          {theme === "light" ? <BsSunFill /> : <TbMoonFilled />}
        </span>
      </div>
    </header>
  );
};

export default Header;
