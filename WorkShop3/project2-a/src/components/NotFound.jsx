import React from "react";
import notFound from "../assets/image/404.png";
const NotFound = () => {
  return (
    <div className="container">
      <h3 className="title">ไม่พบข้อมูล (404 Page Not Found)</h3>
      <img src={notFound} alt="notFound" />
    </div>
  );
};

export default NotFound;
