import nf from "../Images/notfound.jpg";

export default function NotFound() {
  return (
    <div className="container">
      <h2>404 Page Not Found!</h2>
      <img src={nf} alt="home" />
    </div>
  );
}
