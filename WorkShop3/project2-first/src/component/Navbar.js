import React, { Component } from "react";
import Navitem from "./Navitem";
import "./Navbar.css";

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      NavitemActive: "",
    };
  }

  render() {
    return (
      <nav>
        <h2 className="logo"> Blog Appication </h2>

        <Navitem item="Home" tolink="/"></Navitem>
        <Navitem item="About" tolink="/about"></Navitem>
        <Navitem item="Blogs" tolink="/Blogs"></Navitem>
      </nav>
    );
  }
}
export default Navbar;
