import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import { Link } from "react-router-dom";
import "./Navbar.css"

export default function Navbar() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar className="App-bar">
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 4 }} >
            <Link to="/" className="link">CRUD</Link>
          </Typography>
          <Typography variant="h6" component="div" sx={{ flexGrow: 0.2 }}>
            <Link to='/card' className="link">CARD</Link>
          </Typography>
          <Typography variant="h6" component="div" sx={{ flexGrow: 0 }}>
            <Link to='/table' className="link">TABLE</Link>
          </Typography>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
