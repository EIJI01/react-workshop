import { Routes, Route, Navigate} from "react-router-dom";
import Navbar from "./components/Navbar";
import Table from "./components/Table";
import Card from "./components/Card";
import UserCreate from "./components/UserCreate";
import UserEdit from "./components/UserEdit";

export default function App() {
  return (
    <div>
      <Navbar />
      <Routes>
        <Route path="/" element={<Table/>}/>
        <Route path="/card" element={<Card/>}/>
        <Route path="create" element={<UserCreate/>}/>
        <Route path="edit/:id" element={<UserEdit/>}/>
        <Route path="/table" element={<Navigate to="/" />}></Route>
      </Routes>
    </div>
  );
}
