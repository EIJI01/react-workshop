import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import Navbar from './Navbar'
import User from './User'
import {Routes, Route} from "react-router-dom";
import UserCreate from './UserCreate'
import UserUpdate from './UserUpdate'
import Card from './Card'

function App() {

  return (
    <div>
      <Navbar/>
        <Routes>
          <Route path="/card" element={<Card/>}/>
          <Route path="/" element={<User/>}/>
          <Route path="create" element={<UserCreate/>}/>
          <Route path="update/:id" element={<UserUpdate/>}/>

        </Routes>
      
      
    </div>
    
  )
}

export default App
