import "./App.css";
import Navbar from "./components/Navbar.jsx";
import Users from "./components/Users";
import UserCreate from "./components/UserCreate.jsx";
import { Route, Routes } from "react-router-dom";
import { BrowserRouter } from "react-router-dom";
import UserUpdate from "./components/UserUpdate";
import CardInfo from "./components/CardInfo";

function App() {
  return (
    <BrowserRouter>
      <Navbar></Navbar>
      <Routes>
        <Route path="/" element={<Users></Users>}></Route>
        <Route path="/create" element={<UserCreate></UserCreate>}></Route>
        <Route path="/update/:id" element={<UserUpdate></UserUpdate>}></Route>
        <Route path="cardInfo" element={<CardInfo></CardInfo>}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
