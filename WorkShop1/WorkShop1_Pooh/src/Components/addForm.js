import "./addForm.css";
import { useState } from "react";

export default function AddForm(props) {
  const { students, setStudent } = props;
  const [name, setName] = useState("");
  const [gender,setGender] = useState("male");
  function saveStudent(e) {
    e.preventDefault();

    if (!name) {
      alert("Input name!");
    } else {
      const newStudent = {
        id: Math.floor(Math.random() * 100),
        name: name,
        gender:gender,
      };
      setStudent([...students, newStudent]);
      setName("");
    }
  }

  return (
    <section className="container">
      <form onSubmit={saveStudent}>
        <label>Student Name</label>{" "}
        <input
          type="text"
          name="name"
          value={name}
          onChange={(e) => setName(e.target.value)}
        ></input>{" "}
        <select value={gender} onChange={(e)=>setGender(e.target.value)}>
            <option>Male</option>
            <option>Female</option>
        </select>{" "}
        <button type="submit">Record</button>
      </form>
    </section>
  );
}
