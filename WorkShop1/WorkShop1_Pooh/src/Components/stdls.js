import { useState } from "react";
import Item from "./item";

export default function StudentList(props) {
//   const [student, setStudent] = useState([
//     { id: 1, name: "Pooh" },
//     { id: 2, name: "อิอิ" },
//     { id: 3, name: "wow" },
//   ]);
  const [show, setShow] = useState(true);
  const {students,deleteStudent} = props;

//   function deleteStudent(id) {
//     setStudent(student.filter((item) => item.id !== id));
//   }

  const btnStyle = {
    background: show ? "red" : "green",
  };

  return (
    <>
      <h1> std length = {students.length}</h1>
      <button onClick={() => setShow(!show)} style={btnStyle}>
        toggle
      </button>
      <ul>
        {show &&
          students.map((data) => (
            <Item key={data.id} data={data} deleteStudent={deleteStudent}/>
          ))}
      </ul>
    </>
  );
}
