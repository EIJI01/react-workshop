import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Ws01 from './Ws01/Ws01'
import Ws02 from './Ws02/Ws02'
function App() {

  return (
    <>
      <Ws02 />
    </>
  )
}

export default App
