import { Link } from "react-router-dom"
import "./Navbar.css"
export default function Nevbar(){


    return(
        <nav>
            <Link to="/" className="logo">
                <h3>Bolg Application</h3>
            </Link>
            <Link to="/">หน้าแรก</Link>
            <Link to="/about">เกี่ยวกับ</Link>
            <Link to="/blogs">บทความท้งหมด</Link>
        </nav>
    );
}