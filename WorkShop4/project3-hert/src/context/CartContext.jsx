import { createContext, useContext, useEffect, useReducer } from "react";
import products from "../data/product";
import cartReducer from "../reducer/cartReducer";

const CartContext = createContext();
const initState = {
  products: products,
  total: 0,
  amount: 0,
};

export const CartProvider = ({ children }) => {
  const [state, dispatch] = useReducer(cartReducer, initState);

  function formatMoney(money) {
    return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
  }

  function addQuantity(id) {
    console.log("เพิ่มปริมาณสินค้า : " + id);
    dispatch({ type: "ADD", payload: id });
  }

  function subTractQuantity(id) {
    console.log(id);
    dispatch({ type: "SUBTRACT", payload: id });
  }

  function removeItem(id) {
    console.log("ลบรหัสสินค้า = " + id);
    dispatch({ type: "Remove", payload: id });
  }

  useEffect(() => {
    console.log("คำนวณหาผลรวม");
    dispatch({ type: "CALCULATE_TOTAL" });
  }, [state.products]);

  return (
    <CartContext.Provider
      value={{
        ...state,
        formatMoney,
        removeItem,
        addQuantity,
        subTractQuantity,
      }}
    >
      {children}
    </CartContext.Provider>
  );
};

export const useCart = () => {
  return useContext(CartContext);
};
