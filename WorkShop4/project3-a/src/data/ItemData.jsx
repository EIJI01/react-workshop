const ItemData = [
  {
    id: 1,
    image: "./src/assets/product1.png",
    name: "หมวก",
    price: 2000,
    quantity: 1,
  },
  {
    id: 2,
    image: "./src/assets/product2.jpg",
    name: "หูฟัง",
    price: 2000,
    quantity: 1,
  },
  {
    id: 3,
    image: "./src/assets/product3.jpeg",
    name: "กระเป๋า",
    price: 2000,
    quantity: 1,
  },
];

export default ItemData;
