import { createContext, useContext, useReducer, useEffect } from "react";
import products from "../data/products";
import cartReducer from "../reducer/cartReducer";

//สร้าง Context
const CartContext = createContext();
const initState = {
  products: products,
  total: 0,
  amount: 0,
};

export const CartProvider = ({ children }) => {
  const [state, dispatch] = useReducer(cartReducer, initState);

  function removeItem(id) {
    dispatch({ type: "REMOVE", payload: id });
    console.log("ลบรายการสินค้า");
    
  }

  function addQuantity(id) {
    dispatch({ type: "ADD", payload: id });
    console.log("เพิ่มจำนวนสินค้า");
    
  }

  function subtractQuantity(id) {
    dispatch({ type: "SUBTRACT", payload: id });
    console.log("ลดจำนวนสินค้า");
    
  }

  function formatMoney(money) {
    return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
  }

  useEffect(() => {
    console.log("คำนวณหาผลรวม");
    dispatch({ type: "CALCULATE_TOTAL" });
  }, [state.products]);

  return (
    <CartContext.Provider
      value={{
        ...state,
        formatMoney,
        removeItem,
        addQuantity,
        subtractQuantity,
      }}
    >
      {children}
    </CartContext.Provider>
  );
};

//การนำเอา context ไปใช้งานด้านนอก
export const useCart = () => {
  return useContext(CartContext);
};
