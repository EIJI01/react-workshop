import * as React from "react";
import { useState } from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import ArrowBackIosRoundedIcon from "@mui/icons-material/ArrowBackIosRounded";
import { CardMedia } from "@mui/material";
import Rating from "@mui/material/Rating";
import FavoriteBorderRoundedIcon from "@mui/icons-material/FavoriteBorderRounded";
import AddRoundedIcon from "@mui/icons-material/AddRounded";
import RemoveRoundedIcon from "@mui/icons-material/RemoveRounded";
import { Link } from "react-router-dom";

export default function Details() {
  const [count, setCount] = useState(0);

  const handleIncrement = () => {
    setCount(count + 1);
  };
  const removeIncrement = () => {
    setCount(count - 1);
  };

  return (
    <Box sx={{ flexGrow: 1 }}>

      <div>
        <article
          style={{
            background: "white",
            height: "440px",
            background: "white"
          }}
        >
          <IconButton
            size="sm"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ m: 2 }}
          >
          <Link to="/" style={{ color: "#f73378", background: "pink", boxShadow:"0px 0px 0.35rem", padding:"7px", borderRadius:"50px 50px", fontSize:"18px", textAlign:"center" }}>
            <ArrowBackIosRoundedIcon />
          </Link>
 
          </IconButton>
          <Typography style={{ display: "flex", justifyContent: "center" }}>
            <CardMedia
              component="img"
              alt="cones"
              height="sm"
              image="./public/ice.png"
              style={{width: "300px" }}
            />
          </Typography>
        </article>
        <div style={{ display: "flex", justifyContent: "right" }}>
          <button
            variant="contained"
            size="small"
            onClick={removeIncrement}
            style={{
              borderRadius: "5px",
              color: "white",
              border: "none",
              background: "#f50057",
              width: "35px",
              height: "35px",
              marginTop: "12px"
            }}
          >
            <RemoveRoundedIcon />
          </button>
          <p style={{ width: "40px", textAlign: "center", background:"pink",padding:"5px"}}>
            {count}
          </p>
          <button
            variant="contained"
            size="small"
            onClick={handleIncrement}
            style={{
              borderRadius: "5px",
              color: "white",
              border: "none",
              background: "#f50057",
              width: "35px",
              height: "35px",
              marginTop: "12px"
            }}
          >
            <AddRoundedIcon />
          </button>
        </div>
        <article
          style={{
            background: "pink",
            borderRadius: "25px 25px 0px 0px",
            padding: "35px",
            height: "140",
          }}
        >
          <Typography color="#f50057" fontWeight="bold" fontSize="18px">
            Strawberry Ice
          </Typography>
          <Typography color="#f50057" fontSize="25px">
            Cream
          </Typography>
          <Typography gutterBottom variant="h6" component="div">
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <Rating
                name="simple-controlled"
                value={3}
                style={{ color: "#ff9100" }}
              />
              <Typography color="#f50057" fontSize="18px">
                $52.90
              </Typography>
            </div>
            <br />
            <Typography color="#f50057" fontSize="18px">
              Lorem ipsum, dolor sit amet consectetur adipisicing elit. Odit, et
              assumenda eaque error delectus culpa vitae, consequatur dolorum
              vel doloribus suscipit? Cumque sunt voluptatem quisquam ipsam
              similique inventore facilis explicabo!
            </Typography>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                width: "100%",
                marginTop: "25px",
              }}
            >
              <button
                style={{
                  borderRadius: "5px",
                  color: "white",
                  border: "none",
                  background: "#f50057",
                  width: "40px",
                  height: "40px",
                }}
              >
                <FavoriteBorderRoundedIcon />
              </button>
              <button
                style={{
                  borderRadius: "5px",
                  color: "white",
                  border: "none",
                  background: "#f50057",
                  width: "80%",
                  height: "40px",
                  marginLeft: "25px",
                }}
              >
                Add to Cart
              </button>
            </div>
          </Typography>
        </article>
      </div>
    </Box>
  );
}
