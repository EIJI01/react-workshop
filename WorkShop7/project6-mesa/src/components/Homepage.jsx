import * as React from "react";
import "./Homepage.css";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Detail from "./Detail";
import Menu from "./Menu";
import { Avatar } from "@mui/material";
import MenuDetail from "./MenuDetail";
import Link from "@mui/material/Link";
import user from '../assets/user.jpg'

export default function Homepage() {

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="100%">
        <Box
          className="box"
          sx={{
            pt: 4,
            px: 4,
            borderTopRightRadius:0,
            borderTopLeftRadius:0,
            height: { md: "380px", sm: "390px", xs: "400px" },
          }}
        >
          <Box display={"flex"} justifyContent={"space-between"}>
            <Box>
              <Typography
                className="typography"
                sx={{ fontSize: { md: "1.2rem", sm: "1rem", xs: "0.8rem" } }}
              >
                Available Balance
              </Typography>

              <Typography
                className="typography"
                fontWeight={"bold"}
                sx={{ fontSize: { md: "3rem", sm: "2.5rem", xs: "2rem" } }}
              >
                18,460 $
              </Typography>
            </Box>
            <Box>
              <Avatar src={user} alt='user'/>
            </Box>
          </Box>
          <Box
            maxWidth={"100%"}
            display={"flex"}
            sx={{ justifyContent: "space-around", mt: 1 }}
            aria-label="wrapped"
          >
            <Link
              to="#"
              className="link"
              sx={{
                fontWeight: "bold",
                fontSize: { md: "1.2rem", sm: "1rem", xs: "0.7rem" },
                textDecoration:'none', color:"#fff"
              }}
            >
              This Week
            </Link>
            <Link
              to="#"
              className="link"
              sx={{
                fontWeight: "bold",
                fontSize: { md: "1.2rem", sm: "1rem", xs: "0.7rem" },
                textDecoration:'none', color:"#5698f0"
              }}
            >
              This Month
            </Link>
            <Link
              to="#"
              className="link"
              sx={{
                fontWeight: "bold",
                fontSize: { md: "1.2rem", sm: "1rem", xs: "0.7rem" },
                textDecoration:'none', color:"#5698f0"
              }}
            >
              3 Months
            </Link>
            <Link
              to="#"
              className="link"
              sx={{
                fontWeight: "bold",
                fontSize: { md: "1.2rem", sm: "1rem", xs: "0.7rem" },
                textDecoration:'none', color:"#5698f0"
              }}
            >
              6 Months
            </Link>
          </Box>
        </Box>

        <Box position={'relative'} flexWrap={'wrap'}>
          <Detail />
        </Box>
        <Box sx={{ mt: 4 }}>
          <Menu />
        </Box>
        <Box sx={{ mt: 2 }}>
          <MenuDetail />
        </Box>
      </Container>
    </React.Fragment>
  );
}
