import React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Tooltip from "@mui/material/Tooltip";
import "../components/Navbar.css";
import { useTheme } from "@mui/material/styles";

function NavBar({ name, phrase, icon, avatarOrIcon, maxSizeNavbar }) {
  const theme = useTheme();

  return (
    <AppBar
      position="relative"
      sx={{
        height: maxSizeNavbar,
        borderRadius: "30px 30px 0 0",
        bgcolor: "#019dd5",
        boxShadow: 0,
        mb: 0,
        px: 3,
        zIndex: 1,
        [theme.breakpoints.up("sm")]: {
          height: "150px",
          borderRadius: "0",
          position: "unset",
          px: 5,
        },
      }}
    >
      <Toolbar
        disableGutters
        sx={{
          display: "flex",
          justifyContent: "space-between",
          mt: 6,
          alignItems: "center",
          [theme.breakpoints.up("sm")]: {
            mt: 2,
          },
        }}
      >
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
          }}
        >
          <Typography
            variant="h6"
            noWrap
            component="a"
            href="/"
            sx={{
              letterSpacing: ".1rem",
              color: "inherit",
              textDecoration: "none",
              fontSize: "3rem",
              fontWeight: 550,
              [theme.breakpoints.up("sm")]: {
                fontSize: "48px",
              },
            }}
          >
            {name}
          </Typography>
          <Typography
            variant="h6"
            noWrap
            component="a"
            href="/"
            sx={{
              letterSpacing: ".1rem",
              color: "inherit",
              textDecoration: "none",
              fontSize: "1.7rem",
              [theme.breakpoints.up("sm")]: {
                fontSize: "27px",
              },
            }}
          >
            {phrase}
          </Typography>
        </Box>

        <Box
          sx={{
            display: "flex",
            flexGrow: 1,
            justifyContent: "flex-end",
            alignItems: "center",
          }}
        >
          {icon}
          <Tooltip title="Open settings">{avatarOrIcon}</Tooltip>
        </Box>
      </Toolbar>
    </AppBar>
  );
}
export default NavBar;
