import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import Header from './component/Header'
import './App.css'
import Cart from './component/Cart'

function App() {

  return (
    <div className="App">
      <Header/>
      <Cart/>
      
    </div>
  )
}

export default App
