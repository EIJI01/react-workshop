import { Routes, Route} from "react-router-dom";
import Navbar from "./components/Navbar";
import User from "./components/User";
import UserCreate from "./components/UserCreate";
import UserEdit from "./components/UserEdit";

export default function App() {
  return (
    <div>
      <Navbar />
      <Routes>
        <Route path="/" element={<User/>}/>
        <Route path="create" element={<UserCreate/>}/>
        <Route path="edit/:id" element={<UserEdit/>}/>
      </Routes>
    </div>
  );
}
